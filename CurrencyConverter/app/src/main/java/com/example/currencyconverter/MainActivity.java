package com.example.currencyconverter;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText etCad , etCurr ;
    Spinner spCurr0 , spCurr ;
    Button btnConvert , btnClear ;
    List<String> listCurr = new ArrayList<>();
    String fromCurr = "usd" ;
    String toCurr = "usd" ;
    private static DecimalFormat df2 = new DecimalFormat("#.##");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etCad = findViewById(R.id.et_cad);
        etCurr = findViewById(R.id.et_curr);
        spCurr = findViewById(R.id.sp_curr);
        spCurr0 = findViewById(R.id.sp_curr0);
        btnConvert = findViewById(R.id.btn_convert);
        btnClear = findViewById(R.id.btn_clear);
        listCurr.add("usd");
        listCurr.add("cdn");
        listCurr.add("inr");
        listCurr.add("yuan");
        listCurr.add("euro");


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_item, listCurr );
        spCurr.setAdapter(adapter);
        spCurr0.setAdapter(adapter);


        btnConvert.setOnClickListener(this);
        btnClear.setOnClickListener(this);

        spCurr0.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fromCurr = spCurr0.getSelectedItem().toString();
                Toast.makeText(getApplicationContext(),"Converting from "+fromCurr , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spCurr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                toCurr = spCurr.getSelectedItem().toString();
                Toast.makeText(getApplicationContext(),"Converting to "+toCurr , Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        if ( v.getId() == btnConvert.getId() ) {



            if ( etCad != null && etCad.getText() != null && etCad.getText().toString() != null && !etCad.getText().toString().trim().equals("")) {

                if (spCurr.getSelectedItem().toString().equals(spCurr0.getSelectedItem().toString())) {
                    Toast.makeText(getApplicationContext(),"Both Currencies are the same" , Toast.LENGTH_SHORT).show();
                    etCurr.setText(etCad.getText().toString());

                } else if (fromCurr.equals("usd") && toCurr.equals("cdn")) { // USD to CDN
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*1.25)+"");
                }  else if (fromCurr.equals("usd") && toCurr.equals("inr")) { // USD to INR
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*72.1)+"");
                }  else if (fromCurr.equals("usd") && toCurr.equals("yuan")) { // USD to YUAN
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*7.02)+"");
                } else if (fromCurr.equals("usd") && toCurr.equals("euro")) { // USD to EURO
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.91)+"");

                } else if (fromCurr.equals("cdn") && toCurr.equals("usd")) { // CDN to USD
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.75)+"");
                } else if (fromCurr.equals("cdn") && toCurr.equals("inr")) { // CDN to INR
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*54.3)+"");
                } else if (fromCurr.equals("cdn") && toCurr.equals("yuan")) { // CDN to YUAN
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*5.29)+"");
                } else if (fromCurr.equals("cdn") && toCurr.equals("euro")) { // CDN to EURO
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.69)+"");

                } else if (fromCurr.equals("inr") && toCurr.equals("usd")) { // INR to USD
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.014)+"");
                } else if (fromCurr.equals("inr") && toCurr.equals("cdn")) { // INR to CDN
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.019)+"");
                } else if (fromCurr.equals("inr") && toCurr.equals("yuan")) { // INR to YUAN
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.097)+"");
                } else if (fromCurr.equals("inr") && toCurr.equals("euro")) { // INR to EURO
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.013)+"");

                } else if (fromCurr.equals("yuan") && toCurr.equals("usd")) { // YUAN to USD
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.14)+"");
                } else if (fromCurr.equals("yuan") && toCurr.equals("cdn")) { // YUAN to CDN
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.019)+"");
                } else if (fromCurr.equals("yuan") && toCurr.equals("inr")) { // YUAN to INR
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*10.27)+"");
                } else if (fromCurr.equals("yuan") && toCurr.equals("euro")) { // YUAN to EURO
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*0.13)+"");

                } else if (fromCurr.equals("euro") && toCurr.equals("usd")) { // EURO to USD
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*1.1)+"");
                } else if (fromCurr.equals("euro") && toCurr.equals("cdn")) { // EURO to CDN
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*1.46)+"");
                } else if (fromCurr.equals("euro") && toCurr.equals("inr")) { // EURO to INR
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*79.28)+"");
                } else if (fromCurr.equals("euro") && toCurr.equals("euro")) { // EURO to YUAN
                    etCurr.setText(df2.format(Double.valueOf(etCad.getText().toString())*7.72)+"");
                }

            } else {
                Toast.makeText(getApplicationContext(),"Please Enter some amount of Currency" , Toast.LENGTH_SHORT).show();
            }
        }
        if (v.getId() == R.id.btn_clear) {
            etCurr.setText("");
            etCad.setText("");
        }
    }

}
